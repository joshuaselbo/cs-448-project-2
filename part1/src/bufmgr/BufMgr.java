/* ... */

package bufmgr;

import java.io.*;
import java.util.*;
import diskmgr.*;
import global.*;
import chainexception.*;

public class BufMgr implements GlobalConst{

  private HashTable hashTable = new HashTable();
  private int numBuffers;
  private byte[][] bufPool;
  private Frame[] frameTable;
  private ReplacementManager replacer;
  /**
   * Create the BufMgr object.
   * Allocate pages (frames) for the buffer pool in main memory and
   * make the buffer manage aware that the replacement policy is
   * specified by replacerArg (i.e. HL, Clock, LRU, MRU etc.).
   *
   * @param numbufs number of buffers in the buffer pool.
   * @param replacerArg name of the buffer replacement policy.
   */
  public BufMgr(int numbufs, String replacerArg) {
    this.numBuffers = numbufs;
    this.frameTable = new Frame[this.numBuffers];
    this.bufPool = new byte[this.numBuffers][MAX_SPACE];
    this.frameTable = new Frame[this.numBuffers];
    for (int i = 0; i < this.numBuffers; i++) {
      this.frameTable[i] = new Frame();
    }
    this.replacer = new ReplacementManager(this);
    this.replacer.setBufferManager(this);
  }


  /**
   * Pin a page.
   * First check if this page is already in the buffer pool.
   * If it is, increment the pin_count and return a pointer to this
   * page.  If the pin_count was 0 before the call, the page was a
   * replacement candidate, but is no longer a candidate.
   * If the page is not in the pool, choose a frame (from the
   * set of replacement candidates) to hold this page, read the
   * page (using the appropriate method from {\em diskmgr} package) and pin it.
   * Also, must write out the old page in chosen frame if it is dirty
   * before reading new page.  (You can assume that emptyPage==false for
   * this assignment.)
   *
   * @param Page_Id_in_a_DB page number in the minibase.
   * @param page the pointer poit to the page.
   * @param emptyPage true (empty page); false (non-empty page)
   */
  public void pinPage(PageId pin_pgid, Page page, boolean emptyPage) throws PagePinnedException, HashEntryNotFoundException, BufferPoolExceededException, ChainException {
    PageId localPageId = new PageId(-1);
    int j = 0;
    int i = this.hashTable.lookup(pin_pgid);
    if (i < 0) {
      i = this.replacer.pick_victim();
      if (i < 0) {
        page = null;
        throw new ChainException(null, "BUFMGR: REPLACER_ERROR.");
      }
      if ((this.frameTable[i].pageNo.pid != -1) && (this.frameTable[i].dirty == true)) {
        j = 1;
        localPageId.pid = this.frameTable[i].pageNo.pid;
      }
      boolean bool1 = this.hashTable.remove(this.frameTable[i].pageNo);
      if (bool1 != true) {
        throw new ChainException(null, "BUFMGR: HASH_TABLE_ERROR.");
      }
      this.frameTable[i].pageNo.pid = -1;
      this.frameTable[i].dirty = false;
      
      boolean bool2 = this.hashTable.insert(pin_pgid, i);
      
      this.frameTable[i].pageNo.pid = pin_pgid.pid;
      this.frameTable[i].dirty = false;
      if (bool2 != true) {
        throw new ChainException(null, "BUFMGR: HASH_TABLE_ERROR.");
      }
      Page localPage = new Page(this.bufPool[i]);
      if (j == 1) {
        try {
        SystemDefs.JavabaseDB.write_page(localPageId, localPage);
        } catch(IOException e) {}
      }
      if (emptyPage == false || 0 == 0 ) {
        try
        {
          localPage.setpage(this.bufPool[i]);
          
          SystemDefs.JavabaseDB.read_page(pin_pgid, localPage);
        }
        catch (Exception localException)
        {
          bool1 = this.hashTable.remove(this.frameTable[i].pageNo);
          if (bool1 != true) {
            throw new ChainException(localException, "BUFMGR: HASH_TABLE_ERROR.");
          }
          this.frameTable[i].pageNo.pid = -1;
          this.frameTable[i].dirty = false;
          
          bool1 = this.replacer.unpin(i);
          if (bool1 != true) {
            throw new ChainException(localException, "BUFMGR: REPLACER_ERROR.");
          }
          throw new ChainException(localException, "BUFMGR: DB_READ_PAGE_ERROR.");
        }
      }
      page.setpage(this.bufPool[i]);
    }
    else
    {
      page.setpage(this.bufPool[i]);
      this.replacer.pin(i);
    }
  }


  /**
   * Unpin a page specified by a pageId.
   * This method should be called with dirty==true if the client has
   * modified the page.  If so, this call should set the dirty bit
   * for this frame.  Further, if pin_count&gt;0, this method should
   * decrement it. If pin_count=0 before this call, throw an exception
   * to report error.  (For testing purposes, we ask you to throw
   * an exception named PageUnpinnedException in case of error.)
   *
   * @param globalPageId_in_a_DB page number in the minibase.
   * @param dirty the dirty bit of the frame
   */
  public void unpinPage(PageId PageId_in_a_DB, boolean dirty) throws HashEntryNotFoundException, ChainException {
    int i = this.hashTable.lookup(PageId_in_a_DB);
    if (i < 0) {
      throw new HashEntryNotFoundException(null, "BUFMGR: HASH_NOT_FOUND.");
    }
    if (this.frameTable[i].pageNo.pid == -1) {
      throw new ChainException(null, "BUFMGR: BAD_FRAMENO.");
    }
    if (this.replacer.unpin(i) != true) {
      throw new ChainException(null, "BUFMGR: REPLACER_ERROR.");
    }
    if (dirty == true) {
      this.frameTable[i].dirty = dirty;
    }
  }


  /**
   * Allocate new pages.
   * Call DB object to allocate a run of new pages and
   * find a frame in the buffer pool for the first page
   * and pin it. (This call allows a client of the Buffer Manager
   * to allocate pages on disk.) If buffer is full, i.e., you
   * can't find a frame for the first page, ask DB to deallocate
   * all these pages, and return null.
   *
   * @param firstpage the address of the first page.
   * @param howmany total number of allocated new pages.
   *
   * @return the first page id of the new pages.  null, if error.
   */
  public PageId newPage(Page firstpage, int howmany) throws ChainException {
    PageId localPageId = new PageId();
    
    try {
      SystemDefs.JavabaseDB.allocate_page(localPageId, howmany);
    } catch (Exception localException) {
      throw new ChainException(localException, "BufMgr.java: allocate_page() failed");
    }
    try
    {
      pinPage(localPageId, firstpage, true);
    }
    catch (Exception localException)
    {
      for (int i = 0; i < howmany; i++)
      {
        localPageId.pid += i;
        try {
          SystemDefs.JavabaseDB.deallocate_page(localPageId);
        } catch (Exception e) {
          throw new ChainException(e, "BufMgr.java: deallocate_page() failed");
        }
      }
      return null;
    }
    return localPageId;
  }


  /**
   * This method should be called to delete a page that is on disk.
   * This routine must call the method in diskmgr package to
   * deallocate the page.
   *
   * @param globalPageId the page number in the data base.
   */
  public void freePage(PageId globalPageId) throws ChainException {
    int i = this.hashTable.lookup(globalPageId);
    if (i < 0)
    {
      try {
        SystemDefs.JavabaseDB.deallocate_page(globalPageId);
      } catch (Exception localException) {
        throw new ChainException(localException, "BufMgr.java: deallocate_page() failed");
      }
      
      return;
    }
    if (i >= this.numBuffers) {
      throw new ChainException(null, "BUFMGR, BAD_BUFFER");
    }
    try
    {
      this.replacer.free(i);
    }
    catch (Exception localException1)
    {
      throw new ChainException(localException1, "BUFMGR, REPLACER_ERROR");
    }
    try
    {
      this.hashTable.remove(this.frameTable[i].pageNo);
    }
    catch (Exception localException2)
    {
      throw new ChainException(localException2, "BUFMGR, HASH_TABLE_ERROR");
    }
    this.frameTable[i].pageNo.pid = -1;
    this.frameTable[i].dirty = false;
    
    try {
        SystemDefs.JavabaseDB.deallocate_page(globalPageId);
    } catch (Exception localException) {
      throw new ChainException(localException, "BufMgr.java: deallocate_page() failed");
    }
  }


  /**
   * Used to flush a particular page of the buffer pool to disk.
   * This method calls the write_page method of the diskmgr package.
   *
   * @param pageid the page number in the database.
   */
  public void flushPage(PageId pageid) throws PagePinnedException, ChainException {
    int j = 0;
    for (int i = 0; i < this.numBuffers; i++) {
      if (this.frameTable[i].pageNo.pid == pageid.pid)
      {
        if (this.frameTable[i].pin_count() != 0) {
          j++;
        }
        if (this.frameTable[i].dirty != false)
        {
          if (this.frameTable[i].pageNo.pid == -1) {
            throw new ChainException(null, "BUFMGR: INVALID_PAGE_NO");
          }
          pageid.pid = this.frameTable[i].pageNo.pid;
          
          Page localPage = new Page(this.bufPool[i]);
          
          try {
            SystemDefs.JavabaseDB.write_page(pageid, localPage);
            } catch(IOException e) {}
          try
          {
            this.hashTable.remove(pageid);
          }
          catch (Exception localException)
          {
            throw new ChainException(localException, "BUFMGR: HASH_TBL_ERROR.");
          }
          this.frameTable[i].pageNo.pid = -1;
          this.frameTable[i].dirty = false;
        }
        if (j != 0) {
            throw new PagePinnedException(null, "BUFMGR: PAGE_PINNED.");
        }
        break;
      }
    }
  }

  
  /** Flushes all pages of the buffer pool to disk
   */
  public void flushAllPages() throws PagePinnedException, ChainException {
    PageId pageid = new PageId(-1);
    int j = 0;
    for (int i = 0; i < this.numBuffers; i++) {
      if (this.frameTable[i].pin_count() != 0) {
        j++;
      }
      if (this.frameTable[i].dirty != false)
      {
        if (this.frameTable[i].pageNo.pid == -1) {
          throw new ChainException(null, "BUFMGR: INVALID_PAGE_NO");
        }
        pageid.pid = this.frameTable[i].pageNo.pid;
        
        Page localPage = new Page(this.bufPool[i]);
        
        try {
          SystemDefs.JavabaseDB.write_page(pageid, localPage);
          } catch(IOException e) {}
        try
        {
          this.hashTable.remove(pageid);
        }
        catch (Exception localException)
        {
          throw new ChainException(localException, "BUFMGR: HASH_TBL_ERROR.");
        }
        this.frameTable[i].pageNo.pid = -1;
        this.frameTable[i].dirty = false;
      }
    }
    if (j != 0)
      throw new PagePinnedException(null, "BUFMGR: PAGE_PINNED.");
  }


  /** Gets the total number of buffers.
   *
   * @return total number of buffer frames.
   */
  public int getNumBuffers() {
    return this.numBuffers;
  }


  /** Gets the total number of unpinned buffer frames.
   *
   * @return total number of unpinned buffer frames.
   */
  public int getNumUnpinnedBuffers() {
    return this.replacer.getNumUnpinnedBuffers();
  }

  public Frame[] frameTable()
  {
    return this.frameTable;
  }

}

