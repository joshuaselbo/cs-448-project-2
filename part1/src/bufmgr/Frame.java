package bufmgr;

import global.GlobalConst;
import global.PageId;

class Frame
  implements GlobalConst
{
  public PageId pageNo;
  public boolean dirty;
  public int pin_cnt;
  
  public Frame()
  {
    this.pageNo = new PageId();
    this.pageNo.pid = -1;
    this.dirty = false;
    this.pin_cnt = 0;
  }
  
  public int pin_count()
  {
    return this.pin_cnt;
  }
  
  public int pin()
  {
    return ++this.pin_cnt;
  }
  
  public int unpin()
  {
    this.pin_cnt = (this.pin_cnt <= 0 ? 0 : this.pin_cnt - 1);
    
    return this.pin_cnt;
  }
}
