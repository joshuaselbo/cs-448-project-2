package bufmgr;

import global.GlobalConst;
import global.PageId;
import java.io.PrintStream;

class HashTable implements GlobalConst
{
  private static final int HTSIZE = 20;
  private HashTableEntry[] ht = new HashTableEntry[HTSIZE];
  
  private int hash(PageId paramPageId)
  {
    return paramPageId.pid % HTSIZE;
  }
  
  public HashTable()
  {
    for (int i = 0; i < HTSIZE; i++) {
      this.ht[i] = null;
    }
  }
  
  public boolean insert(PageId paramPageId, int paramInt)
  {
    HashTableEntry localHashTableEntry = new HashTableEntry();
    int i = hash(paramPageId);
    
    localHashTableEntry.pageNo.pid = paramPageId.pid;
    localHashTableEntry.frameNo = paramInt;
    
    localHashTableEntry.next = this.ht[i];
    this.ht[i] = localHashTableEntry;
    
    return true;
  }
  
  public int lookup(PageId paramPageId)
  {
    if (paramPageId.pid == -1) {
      return -1;
    }
    for (HashTableEntry localHashTableEntry = this.ht[hash(paramPageId)]; localHashTableEntry != null; localHashTableEntry = localHashTableEntry.next) {
      if (localHashTableEntry.pageNo.pid == paramPageId.pid) {
        return localHashTableEntry.frameNo;
      }
    }
    return -1;
  }
  
  public boolean remove(PageId paramPageId)
  {
    Object localObject = null;
    if (paramPageId.pid == -1) {
      return true;
    }
    int i = hash(paramPageId);
    HashTableEntry pageToRemove = null;
    for (HashTableEntry localHashTableEntry = this.ht[i]; localHashTableEntry != null; localHashTableEntry = localHashTableEntry.next) {
      if (localHashTableEntry.pageNo.pid == paramPageId.pid) {
        pageToRemove = localHashTableEntry;
        break;
      }
      localObject = localHashTableEntry;
    }
    if (pageToRemove != null)
    {
      if (localObject != null) {
        ((HashTableEntry)localObject).next = pageToRemove.next;
      } else {
        this.ht[i] = pageToRemove.next;
      }
    }
    else
    {
      System.err.println("ERROR: Page " + paramPageId.pid + " was not found in hashtable.\n");
      
      return false;
    }
    return true;
  }
  
  public void display()
  {
    System.out.println("HASH Table contents :FrameNo[PageNo]");
    for (int i = 0; i < HTSIZE; i++) {
      if (this.ht[i] != null)
      {
        for (HashTableEntry localHashTableEntry = this.ht[i]; localHashTableEntry != null; localHashTableEntry = localHashTableEntry.next) {
          System.out.println(localHashTableEntry.frameNo + "[" + localHashTableEntry.pageNo.pid + "]-");
        }
        System.out.println("\t\t");
      }
      else
      {
        System.out.println("NONE\t");
      }
    }
    System.out.println("");
  }

  class HashTableEntry
  {
    public HashTableEntry next;
    public PageId pageNo = new PageId();
    public int frameNo;
  }
}
