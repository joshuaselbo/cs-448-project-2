package bufmgr;

import global.GlobalConst;
import java.io.PrintStream;

class ReplacementManager implements GlobalConst {
  protected BufMgr mgr;
  protected int head;
  protected int[] state_bit;
  public static final int Available = 12;
  public static final int Referenced = 13;
  public static final int Pinned = 14;
  private int[] frames = null;
  private int nframes;

  protected ReplacementManager(BufMgr paramBufMgr)
  {
    this.mgr = paramBufMgr;
    int i = paramBufMgr.getNumBuffers();
    this.state_bit = new int[i];
    for (int j = 0; j < i; j++) {
      this.state_bit[j] = 0;
    }
    this.head = -1;

    this.frames = new int[paramBufMgr.getNumBuffers()];
    this.nframes = 0;
  }
  
  public void pin(int paramInt) throws BufferPoolExceededException
  {
    if ((paramInt < 0) || (paramInt >= this.mgr.getNumBuffers())) {
      throw new BufferPoolExceededException(null, "BUFMGR: Tried to pin a page to the buffer pool with no unpinned frame left.");
    }
    this.mgr.frameTable()[paramInt].pin();
    this.state_bit[paramInt] = 14;

    this.update(paramInt);
  }
  
  public boolean unpin(int paramInt)
  {
    // if ((paramInt < 0) || (paramInt >= this.mgr.getNumBuffers())) {
    //   throw new InvalidFrameNumberException(null, "BUFMGR: BAD_BUFFRAMENO.");
    // }
    // if (this.mgr.frameTable()[paramInt].pin_count() == 0) {
    //   throw new PageUnpinnedException(null, "BUFMGR: PAGE_NOT_PINNED.");
    // }
    this.mgr.frameTable()[paramInt].unpin();
    if (this.mgr.frameTable()[paramInt].pin_count() == 0) {
      this.state_bit[paramInt] = 13;
    }
    return true;
  }
  
  public void free(int paramInt) throws PagePinnedException
  {
    if (this.mgr.frameTable()[paramInt].pin_count() > 1) {
      throw new PagePinnedException(null, "BUFMGR: Tried to free a page that is still pinned.");
    }
    this.mgr.frameTable()[paramInt].unpin();
    this.state_bit[paramInt] = 12;
  }
  
  public int getNumUnpinnedBuffers()
  {
    int i = this.mgr.getNumBuffers();
    int j = 0;
    for (int k = 0; k < i; k++) {
      if (this.mgr.frameTable()[k].pin_count() == 0) {
        j++;
      }
    }
    return j;
  }
  
  protected void setBufferManager(BufMgr paramBufMgr)
  {
    this.mgr = paramBufMgr;
    int i = this.mgr.getNumBuffers();
    for (int j = 0; j < i; j++) {
      this.state_bit[j] = 12;
    }
    this.head = -1;

    this.frames = new int[paramBufMgr.getNumBuffers()];
    this.nframes = 0;
  }
  private void update(int paramInt)
  {
    int i = 0;
    while (this.frames[i] != paramInt)
    {
      i++;
      if (i >= this.nframes) {
        break;
      }
    }
    if (i == 0)
      i++;
    do
    {
      this.frames[(i - 1)] = this.frames[i];
      i++;
    } while (i < this.nframes);
    this.frames[(this.nframes - 1)] = paramInt;
  }
  
  public int pick_victim()
  {
    int i = this.mgr.getNumBuffers();
    int j;
    if (this.nframes < i)
    {
      j = this.nframes++;
      this.frames[j] = j;
      this.state_bit[j] = 14;
      this.mgr.frameTable()[j].pin();
      return j;
    }
    for (int k = 0; k < i; k++)
    {
      j = this.frames[k];
      if (this.state_bit[j] != 14)
      {
        this.state_bit[j] = 14;
        this.mgr.frameTable()[j].pin();
        update(j);
        return j;
      }
    }
    return -1;
  }
}
