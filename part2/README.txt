CS 448 Project 2 - Part 2 README

Code located in: src/heap/
Test case output: test-out.txt

--Group Members--
Hunter Martin
Joshua Selbo

--Assumptions--

Our implementation of Heapfile is a doubly-linked list of HFPages, natively supported
by the next and previous PageId pointers in the HFPage class.

All contextual assumptions are provided in code comments.

--Other notes--

The following classes are already provided as .class files, so we
do not provide a .java implementation of these classes:

ConstSlot
FieldNumberOutOfBoundsException
HFPage
InvalidSlotNumberException
InvalidTupleSizeException
InvalidTypeException
Tuple

In order to be in strict accordance with the given API, we implement the following
exception classes, although they may or may not be thrown by the corresponding
throwing methods:

FileAlreadyDeletedException
HFBufMgrException
HFDiskMgrException
HFException
InvalidUpdateException
SpaceNotAvailableException

We also provide the Filetype protocol in order to conform to the API, but it
is not used by our implementation.
