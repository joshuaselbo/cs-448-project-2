package heap;

import chainexception.ChainException;

/**
 * @author Joshua Selbo
 */
public class HFBufMgrException extends ChainException {
  public HFBufMgrException() {
    super();
  }

  public HFBufMgrException(Exception ex, String name) {
    super(ex, name);
  }
}

