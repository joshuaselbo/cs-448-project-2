package heap;

import chainexception.ChainException;

/**
 * @author Joshua Selbo
 */
public class HFDiskMgrException extends ChainException {
  public HFDiskMgrException() {
    super();
  }

  public HFDiskMgrException(Exception ex, String name) {
    super(ex, name);
  }
}

