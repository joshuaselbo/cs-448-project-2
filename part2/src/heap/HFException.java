package heap;

import chainexception.ChainException;

/**
 * @author Joshua Selbo
 */
public class HFException extends ChainException {
  public HFException() {
    super();
  }

  public HFException(Exception ex, String name) {
    super(ex, name);
  }
}

