package heap;

import bufmgr.BufMgr;
import bufmgr.PageNotReadException;

import chainexception.ChainException;

import diskmgr.DB;
import diskmgr.DiskMgrException;
import diskmgr.Page;

import global.GlobalConst;
import global.PageId;
import global.RID;
import global.SystemDefs;

import java.io.IOException;

/**
 * @author Joshua Selbo
 */
public class Heapfile implements Filetype, GlobalConst {
  // This must be accessible to Scan, so we make it package private.
  /* package-private */ PageId headPageId;

  private String name;
  private DB db;
  private BufMgr bm;
  private boolean deleted = false;

  public Heapfile(String name) throws HFException,
      HFBufMgrException, HFDiskMgrException, IOException {
    this.name = name;

    db = SystemDefs.JavabaseDB;
    bm = SystemDefs.JavabaseBM;

    initialize();
  }

  public int getRecCnt() throws InvalidSlotNumberException, InvalidTupleSizeException,
      HFDiskMgrException, HFBufMgrException, IOException {
    int count = 0;

    PageId currPageId = new PageId(headPageId.pid);
    HFPage currPage = new HFPage();
    try {
      while (currPageId.pid != -1) {
        bm.pinPage(currPageId, currPage, false);

        count += currPage.getSlotCnt();

        PageId nextPageId = currPage.getNextPage();
        bm.unpinPage(currPageId, false);
        currPageId = new PageId(nextPageId.pid);
      }
    } catch (ChainException ce) {
      throw new HFBufMgrException(ce, "Failed to get record count");
    }

    return count;
  }

  public RID insertRecord(byte[] recPtr) throws InvalidSlotNumberException,
      InvalidTupleSizeException, SpaceNotAvailableException, HFException,
      HFBufMgrException, HFDiskMgrException, IOException {
    // Check that max page size can hold recPtr. Subtract 4 for slot size
    if (recPtr.length > MINIBASE_PAGESIZE - 4) {
      throw new SpaceNotAvailableException(null, "recPtr length too long");
    }

    PageId pageId = null;
    try {
      pageId = findNextAvailablePage(recPtr.length);
      // If there is no page with sufficient space, insert a new page
      if (pageId == null) {
        pageId = insertNewPage();
      }
    } catch (ChainException ce) {
      throw new HFBufMgrException(ce, "Failed to find or insert available page");
    }

    RID ret = null;
    try {
      HFPage hfPage = new HFPage();
      bm.pinPage(pageId, hfPage, false);

      ret = hfPage.insertRecord(recPtr);

      bm.unpinPage(pageId, true);
    } catch (ChainException ce) {
      throw new HFBufMgrException(ce, "Failed to pin or unpin page");
    }

    return ret;
  }

  /**
   * Optimize by speed, rather than space, efficiency.
   * In the case that a deletion results in a page with 0 records,
   * we do not delete the empty page - we allow it to remain in the linked list.
   */
  public boolean deleteRecord(RID rid) throws InvalidSlotNumberException,
      InvalidTupleSizeException, HFException, HFBufMgrException,
      HFDiskMgrException, Exception {
    HFPage page = new HFPage();
    try {
      bm.pinPage(rid.pageNo, page, false);
    } catch (PageNotReadException pnfe) {
      // Indicates we don't have a corresponding page for rid.pageNo
      return false;
    }

    // Check rid.slotNo in valid range
    if (rid.slotNo < 0 || rid.slotNo >= page.getSlotCnt()) {
      bm.unpinPage(rid.pageNo, true);
      return false;
    }

    page.deleteRecord(rid);
    bm.unpinPage(rid.pageNo, true);
    return true;
  }

  public boolean updateRecord(RID rid, Tuple newtuple) throws InvalidSlotNumberException,
      InvalidUpdateException, InvalidTupleSizeException, HFException,
      HFDiskMgrException, HFBufMgrException, Exception {
    HFPage page = new HFPage();
    try {
      bm.pinPage(rid.pageNo, page, false);
    } catch (PageNotReadException pnfe) {
      // Indicates we don't have a corresponding page for rid.pageNo
      return false;
    }

    // Check rid.slotNo in valid range
    if (rid.slotNo < 0 || rid.slotNo >= page.getSlotCnt()) {
      bm.unpinPage(rid.pageNo, true);
      return false;
    }

    // There is no updateRecord method in HFPage for some reason, so
    // we have to do some manual bit fiddling
    byte[] data = page.getHFpageArray();
    int slotLen = page.getSlotLength(rid.slotNo);
    int slotOffset = page.getSlotOffset(rid.slotNo);

    byte[] tupleData = newtuple.returnTupleByteArray();
    int tupleLen = newtuple.getLength();
    int tupleOffset = newtuple.getOffset();

    // Check slot length is same as tuple
    if (slotLen != tupleLen) {
      throw new InvalidUpdateException(null, "Slot length and tuple length not equal");
    }

    // Copy new tuple data
    System.arraycopy(tupleData, tupleOffset, data, slotOffset, tupleLen);
    
    bm.unpinPage(rid.pageNo, true);
    return true;
  }

  public Tuple getRecord(RID rid) throws InvalidSlotNumberException,
      InvalidTupleSizeException, HFException, HFDiskMgrException,
      HFBufMgrException, Exception {
    Tuple ret = null;
    try {
      HFPage page = new HFPage();
      bm.pinPage(rid.pageNo, page, false);
      ret = page.getRecord(rid);
      bm.unpinPage(rid.pageNo, false);
    } catch (ChainException ce) {
      throw new HFBufMgrException(ce, "Failed to get record");
    }
    return ret;
  }

  public Scan openScan() throws InvalidTupleSizeException, IOException {
    return new Scan(this);
  }

  public void deleteFile() throws InvalidSlotNumberException,
      FileAlreadyDeletedException, InvalidTupleSizeException,
      HFBufMgrException, HFDiskMgrException, IOException {
    if (deleted) {
      throw new FileAlreadyDeletedException();
    }

    // Free all pages in this Heapfile
    HFPage page = new HFPage();
    PageId currPageId = new PageId(headPageId.pid);
    try {
      while (currPageId.pid != -1) {
        bm.pinPage(currPageId, page, false);
        PageId nextPageId = page.getNextPage();
        bm.freePage(currPageId);
        currPageId = new PageId(nextPageId.pid);
      }
    } catch (ChainException ce) {
      throw new HFBufMgrException(ce, "Failed to free pages");
    }
    
    try {
      db.delete_file_entry(name);
    } catch (ChainException ce) {
      throw new HFDiskMgrException(ce, "Failed to delete file entry");
    }

    deleted = true;
  }

  private void initialize() throws HFException,
      HFBufMgrException, HFDiskMgrException, IOException {
    try {
      // First, attempt to retrieve existing file entry in database
      headPageId = db.get_file_entry(name);
    } catch (ChainException ce) {
      // All exceptions are DiskMgrExceptions
      throw new HFDiskMgrException(ce, "Failed to open DB file entry");
    }

    if (headPageId == null) {
      // If failed, then this is the first request to open a Heapfile with this name.
      // We should create a new HFPage and add a file entry to the database.
      try {
        HFPage headPage = new HFPage();
        Page page = new Page();
        headPageId = bm.newPage(page, 1);
        headPage.init(headPageId, page);
        bm.unpinPage(headPageId, true);
      } catch (DiskMgrException dme) {
        throw new HFDiskMgrException(dme, "Failed to create head HFPage");
      } catch (ChainException ce) {
        // All other exceptions are BufMgr exceptions
        throw new HFBufMgrException(ce, "Failed to create head HFPage");
      }
      if (headPageId == null) {
        // Something has gone wrong, but the method failed to throw an
        // exception.
        throw new HFException(null, "Failed to create head HFPage");
      }

      try {
        db.add_file_entry(name, headPageId);
      } catch (ChainException ce) {
        // All exceptions are BufMgr exceptions
        throw new HFBufMgrException(ce, "Failed to add DB file entry");
      }
    }
  }

  /**
   * Searches through linked list of pages and returns the first page
   * with sufficient space to store a record of the given length.
   *
   * If no such page is found, returns null.
   */
  private PageId findNextAvailablePage(int len) throws ChainException, IOException {
    PageId currPageId = new PageId(headPageId.pid);
    HFPage currPage = new HFPage();
    while (currPageId.pid != -1) {
      bm.pinPage(currPageId, currPage, false);

      boolean foundPage = (currPage.available_space() >= len);

      PageId nextPageId = currPage.getNextPage();
      bm.unpinPage(currPageId, false);

      if (foundPage) {
        return currPageId;
      }

      currPageId = new PageId(nextPageId.pid);
    }

    // We have not found an available page.
    return null;
  }

  /**
   * Insertas a new page and returns its PageId. Guaranteed to return non-null PageId.
   */
  private PageId insertNewPage() throws ChainException, IOException {
    // Find last page in linked list
    PageId currPageId = new PageId(headPageId.pid);
    HFPage currPage = new HFPage();
    while (true) {
      bm.pinPage(currPageId, currPage, false);
      PageId nextPageId = currPage.getNextPage();
      if (nextPageId.pid == -1) {
        break;
      }
      bm.unpinPage(currPageId, false);
      currPageId = new PageId(nextPageId.pid);
    }

    // Create new page
    HFPage newPage = new HFPage();
    Page page = new Page();
    PageId newPageId = bm.newPage(page, 1);
    if (newPageId == null) {
      throw new HFBufMgrException(null, "Buffer manager returned a null new page ID");
    }
    newPage.init(newPageId, page);

    // Update refs
    currPage.setNextPage(new PageId(newPageId.pid));
    newPage.setPrevPage(new PageId(currPageId.pid));

    bm.unpinPage(newPageId, true);
    bm.unpinPage(currPageId, true);

    return newPageId;
  }
}
