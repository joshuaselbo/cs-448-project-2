package heap;

import bufmgr.BufMgr;

import chainexception.ChainException;

import global.GlobalConst;
import global.PageId;
import global.RID;
import global.SystemDefs;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Joshua Selbo
 */
public class Scan implements GlobalConst {
  private Heapfile hf;
  private BufMgr bm;

  private HFPage currPage;
  private RID currRid;
  private int recordCount;

  public Scan(Heapfile hf) throws InvalidTupleSizeException, IOException {
    this.hf = hf;
    bm = SystemDefs.JavabaseBM;

    currPage = new HFPage();
    try {
      recordCount = hf.getRecCnt();

      bm.pinPage(hf.headPageId, currPage, false);
      currRid = currPage.firstRecord();
    } catch (ChainException ce) {
      throw new IOException(ce);
    }
  }

  public void closescan() {
    // The API does not allow this method to throw exceptions...
    try {
      safelyCloseScan();
    } catch (ChainException ce) {
      // Consume, but don't propagate.
      ce.printStackTrace();
    } catch (IOException ioe) {
      // Consume, but don't propagate.
      ioe.printStackTrace();
    }
  }
    

  public Tuple getNext(RID rid) throws InvalidTupleSizeException, IOException {
    if (currRid == null) {
      // We are done iterating.
      try {
        // Force unpin of currPage in accordance with test expectation
        safelyCloseScan();
      } catch (ChainException ce) {
        ce.printStackTrace();
      }
      return null;
    }

    Tuple ret = null;
    try {
      ret = currPage.getRecord(currRid);
      // Copy out current RID to parameter passed in.
      rid.copyRid(currRid);

      // Ask for next record
      currRid = currPage.nextRecord(currRid);
      if (currRid == null) {
        // Done iterating this page, proceed to next page
        PageId nextPageId = currPage.getNextPage();

        if (nextPageId.pid != -1) {
          bm.unpinPage(currPage.getCurPage(), false);
          bm.pinPage(nextPageId, currPage, false);
          currRid = currPage.firstRecord();
        }
        // else, we have finished the last page and we are done iterating.
      }
    } catch (ChainException ce) {
      // Consume, don't propagate, return null.
      ce.printStackTrace();
      return null;
    }

    return ret;
  }

  /**
   * If rid.pageNo is invalid, this method will fail (print trace and return false).
   * However, we do not validate rid.slotNo in this method. If rid.slotNo is invalid,
   * the getNext(RID) method will fail on the next call (print trace and return null).
   */
  public boolean position(RID rid) throws InvalidTupleSizeException, IOException {
    try {
      // Attempt to pin new page
      HFPage newPage = new HFPage();
      bm.pinPage(rid.pageNo, newPage, false);

      // Okay, now release old page and update variables
      bm.unpinPage(currPage.getCurPage(), false);
      currPage = newPage;
      currRid.copyRid(rid);
    } catch (ChainException ce) {
      // Consume, don't propagate, return false.
      ce.printStackTrace();
      return false;
    }
    return true;
  }

  private void safelyCloseScan() throws ChainException, IOException {
    if (currPage != null) {
      bm.unpinPage(currPage.getCurPage(), false);
      currPage = null;
    }
  }
}
