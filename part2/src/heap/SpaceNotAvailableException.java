package heap;

import chainexception.ChainException;

/**
 * @author Joshua Selbo
 */
public class SpaceNotAvailableException extends ChainException {
  public SpaceNotAvailableException() {
    super();
  }

  public SpaceNotAvailableException(Exception ex, String name) {
    super(ex, name);
  }
}

